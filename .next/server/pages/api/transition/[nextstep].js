"use strict";
(() => {
var exports = {};
exports.id = 401;
exports.ids = [401];
exports.modules = {

/***/ 892:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ handler)
/* harmony export */ });
// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
let currentItem = 'blue';
let isVisitedYellow = false;
function handler(req, res) {
  const {
    nextstep
  } = req.query;

  if (nextstep == 'reset') {
    currentItem = 'blue';
    isVisitedYellow = false;
  } else if (currentItem != nextstep) {
    if (currentItem != 'blue' && nextstep != 'blue') {
      return res.status(400).send({
        status: 400,
        data: {
          currentItem,
          isVisitedYellow
        },
        msg: 'current state is ' + currentItem + ' is invalid'
      });
    }

    if (isVisitedYellow) {
      if (nextstep === 'yellow') {
        return res.status(400).send({
          status: 400,
          data: {
            currentItem,
            isVisitedYellow
          },
          msg: 'You cannot visit Yellow consecutively'
        });
      }

      if (nextstep === 'green') {
        isVisitedYellow = false;
      }
    } else {
      if (nextstep === 'yellow') {
        isVisitedYellow = true;
      }
    }

    currentItem = nextstep;
  }

  res.send({
    status: 200,
    msg: 'ok',
    data: {
      currentItem,
      isVisitedYellow
    }
  });
}

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(892));
module.exports = __webpack_exports__;

})();