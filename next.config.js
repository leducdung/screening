/** @type {import('next').NextConfig} */
if (process.env.NODE_ENV == 'production'){
  module.exports = {
    reactStrictMode: true,
    env: {
      domain: 'https://lddung.netlify.app',
    },
  }
  
}
else if (process.env.NODE_ENV == 'development'){
  module.exports = {
    reactStrictMode: true,
    env: {
      domain: 'http://localhost:3000',
    },
  }
}
