import React from "react";

interface ButtonProps {
  onClick: () => void;
  text?: string;
  isSelected: boolean,
  className?: string;
}

const Button = ({
    onClick,
    isSelected = false,
    text = "",
    className = "",
}: ButtonProps) => (
    <button
      className={isSelected ? `${className} btn-selected` : className }
      onClick={onClick}
    >
      {text}
  </button>
);

export default Button;