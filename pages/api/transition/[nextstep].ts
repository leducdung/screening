// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'

type Data = {
    status: number,
    data: Object
    msg: string
}

let currentItem = 'blue';
let isVisitedYellow = false;

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<Data>
    ) {
    const { nextstep } = req.query

    if (nextstep == 'reset'){
        currentItem = 'blue';
        isVisitedYellow = false;
    }

    else if (currentItem != nextstep){
        if (currentItem != 'blue' && nextstep != 'blue') {
            return res.status(400).send({ status: 400, data: { currentItem, isVisitedYellow }, msg: 'current state is '+ currentItem +' is invalid' });
        }

        if (isVisitedYellow){
            if (nextstep === 'yellow'){
                return res.status(400).send({ status: 400, data: { currentItem, isVisitedYellow }, msg: 'You cannot visit Yellow consecutively' });
            }
            if (nextstep === 'green'){
                isVisitedYellow = false;
            }
        }
        
        else{
            if (nextstep === 'yellow') {
                isVisitedYellow = true;
            }
        }

        currentItem = nextstep as string;
    }

    
    res.send({
        status: 200,
        msg: 'ok',
        data: {
            currentItem,
            isVisitedYellow,
        }
    });
}