import type { NextPage } from 'next'
import { useCallback, useEffect, useState } from 'react'
import Head from 'next/head'
import Button from "./components/button";
import styles from '../styles/Home.module.css'

interface Props {
	status: number,
	msg: string;
	currentItem: string;
}

const Home: NextPage<Props> = ({ status, currentItem, msg }) => {

	const [initState, setInitState] = useState([ 'green', 'blue', 'yellow'	])
	const [state, setState] = useState({ status, currentItem, msg })

	const handleReset = async() => {
		const res = await fetch(`${process.env.domain}/api/transition/reset`)
		const json = await res.json()
		
		return setState({ status, currentItem: json.data.currentItem, msg: json.msg })
	};

	const handleNextTransition = async(param: string) => {
		const res = await fetch(`${process.env.domain}/api/transition/${param}`)
		const json = await res.json()
		return setState({ status, currentItem: json.data.currentItem, msg: json.msg })
	};


	return (
		<div className={styles.container}>

			<main className={styles.main}>

				{initState.map((value, index)=>(
					<Button
						key={`${value}_${index}`}
						className={`btn btn-shape__circle btn-color__${value}`}
						isSelected={value === state.currentItem ? true : false}
						onClick={() => handleNextTransition(value)}
					/>

				))}
				<Button
					text="Reset"
					className="btn btn-reset"
					isSelected={false}
					onClick={handleReset}
				/>

			</main>

			{state.msg != 'ok' && <p className={styles.error}>{state.msg}</p>}

		</div>
  )

} 	

Home.getInitialProps = async (ctx) => {
	const res = await fetch(process.env.domain + '/api/transition/reset')
	const json = await res.json()
	return { status: json.status, currentItem: json.data.currentItem, msg: json.msg }

}

export default Home
